/**
 * Variables
 *
 */
var display = document.getElementById('display'),
    inputs = document.getElementsByClassName('inputs'),
    operators = document.getElementsByClassName('operators'),
    equal = document.getElementById('equal'),
    clear = document.getElementById('clear'),
    percent = document.querySelector('.percent'),
    backspace = document.getElementById('backspace'),
    reset = false,
    currentInputValue,
    currentOperator,
    displayValue,
    result,
    backspaceValue,
    i,
    io;

/**
 * Functions
 *
 */
// for data (numeric values) input
function dataInput() {
  clearAfterCalc();
  currentInputValue = this.value;
  display.value += currentInputValue;
}

// for operator ('+', '-', '*', '/') input
function operatorInput() {
  currentOperator = this.value;
  display.value += currentOperator;
}

// for percentage calculation
function percentCalc() {
  display.value = (display.value) / 100;
}

// for displaying the calculated result
function displayResult() {
  if (display.value === "") {
    display.value = "";
  }
  else {
    displayValue = display.value;
    displayValue = displayValue.replace(/\^/g, '\*\*') || displayValue.replace(/[\d.]+/g, function (n) {
      return parseFloat(n);
    });
    result = eval(displayValue);
    display.value = result;
    reset = true;
  }
}

// for deleting (backspace) single value
function deleteSingle() {
  backspaceValue = display.value;
  display.value = backspaceValue.substr(0, backspaceValue.length - 1);
}

// for clearing input field
function clearAll() {
  display.value = "";
}

// for clearing after output of result
function clearAfterCalc() {
  if (reset) {
    display.value = '';
    reset = false;
  }
}

// for blocking alphabets into input field and helping calculation through keyboard keys
function keyboardInput(key) {  
  clearAfterCalc();
  
  if ((key.which < 0 || key.which > 57) && (key.which !== 13 && key.which !== 99 && key.which !== 94 && key.which !== 37)) {
    return false;
  } else {
    key.preventDefault();
    if (key.which === 48) {
      display.value += "0";
    } else if (key.which === 49) {
      display.value += "1";
    } else if (key.which === 50) {
      display.value += "2";
    } else if (key.which === 51) {
      display.value += "3";
    } else if (key.which === 52) {
      display.value += "4";
    } else if (key.which === 53) {
      display.value += "5";
    } else if (key.which === 54) {
      display.value += "6";
    } else if (key.which === 55) {
      display.value += "7";
    } else if (key.which === 56) {
      display.value += "8";
    } else if (key.which === 57) {
      display.value += "9";
    } else if (key.which === 46) {
      display.value += ".";
    } else if (key.which === 42) {
      display.value += "*";
    } else if (key.which === 47) {
      display.value += "/";
    } else if (key.which === 43) {
      display.value += "+";
    } else if (key.which === 45) {
      display.value += "-";
    } else if (key.which === 94) {
      display.value += "^";
    } else if (key.which === 37) {
      percentCalc();
    } else if (key.which === 13) {
      displayResult();
      reset = true;
    } else if (key.which === 99) { // Press C on keyboard to clear the screen
      clearAll();
    } else {
      display.value = display.value;
    }
    return true;
  }
}

// for deleting value using backspace
function backspaceKeyEvent (event) {
  if (event.which === 8) {
    deleteSingle();
  }
}

/**
 * Code execution
 *
 */
window.onload = function () {
  document.onkeypress = keyboardInput;
  document.onkeydown = backspaceKeyEvent;

  // for data(numeric values) input
  for (i = 0; i < inputs.length; i++) {
    inputs[i].onclick = dataInput;
  }
  // for operator(+-*/) input
  for (io = 0; io < operators.length; io++) {
    operators[io].onclick = operatorInput;
  }

  percent.onclick = percentCalc;
  equal.onclick = displayResult;
  backspace.onclick = deleteSingle;
  clear.onclick = clearAll;
};